/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aurora.ws;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Luis Caamaño
 */
@javax.ws.rs.ApplicationPath("ws")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(aurora.ws.BavaroResource.class);
        resources.add(aurora.ws.LoginResource.class);
//        resources.add(aurora.ws.TrackerListResource.class);
//        resources.add(aurora.ws.TrackerResource.class);
//        resources.add(aurora.ws.TrasladosResource.class);
        resources.add(aurora.ws.NewCrossOriginResourceSharingFilter.class);
    }
    
}
