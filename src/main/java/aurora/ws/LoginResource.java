/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aurora.ws;

import static Navixy.NavixyAPICalls.navixyAPILogin;
import com.google.gson.Gson;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Luis Caamaño
 */
@Path("login")
public class LoginResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of GenericResource
     */
    public LoginResource() {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{username}/{password}")
    public String getJson(@PathParam("username") String username,@PathParam("password") String password) throws IOException, NoSuchAlgorithmException {
        //TODO return proper representation object
        Gson gson = new Gson();
        Map<String, Object> user = navixyAPILogin(username, password);
        if(user.containsKey("hash")){
            user.put("success", "true");
            return   gson.toJson(user);
        } else {
          return   gson.toJson(user);
        }
    }
    
   

    /**
     * PUT method for updating or creating an instance of GenericResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public String putJson(String content) {
                return "GETTER";

    }
}
