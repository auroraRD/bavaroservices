/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aurora.ws;

import Navixy.NavixyAPICalls;
import static Navixy.NavixyAPICalls.listNavixyTrackers;
import static Navixy.NavixyAPICalls.navixyGetTrackRead;
import static Navixy.NavixyAPICalls.navixyGetTrackers;
import static Navixy.NavixyAPICalls.navixyGetUserInfo;
import static Navixy.NavixyAPICalls.navixyGetZoneList;
import static Navixy.NavixyAPICalls.navixyGetZonePointsList;
import static Navixy.NavixyAPICalls.navixyHistoryTrackerList;
import static Navixy.NavixyAPICalls.navixyRuleList;
import static Navixy.NavixyAPICalls.navixyTrackerGetLocation;
import static Navixy.NavixyAPICalls.navixyTrackerGetDiagnostics;
import static Navixy.NavixyAPICalls.navixyTrackerGetState;
import static Navixy.NavixyAPICalls.utilDoubleIntStr;
//import static Navixy.NavixyAPICalls.navixyGetTrackRead;
import Navixy.NavixyJsonResponse;
import Navixy.NavixyTracker;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Luis Caamaño
 */
@Path("bavaro")
public class BavaroResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of GenericResource
     */
    public BavaroResource() {
    }

//=====================================================================================================
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("camion/puntos/{hash}/{trackerid}/{fechaini}/{fechafin}")
    //public String camionPuntos(@PathParam("hash",) String hash, @PathParam("trackerid") String trackerid, @PathParam("fechaini") String fechaini,  @PathParam("fechafin")String fechafin ) {
    public String camionPuntos(@PathParam("hash") String hash, @PathParam("trackerid") String trackerid, @PathParam("fechaini") String fechaini, @PathParam("fechafin") String fechafin) {

        Gson gson = new Gson();

        Map<String, Object> userInfo = navixyGetUserInfo(hash);
        Object validateHashStr = userInfo.get("success");
        if ((validateHashStr != null) & (String.valueOf(validateHashStr).equals("true"))) {

            Map<String, Object> mapPuntosService = navixyGetTrackRead(hash, trackerid, fechaini, fechafin, "");

            // Map<String,Object> mapReglasService = navixyRuleList(hash);
            if (mapPuntosService != null & mapPuntosService.get("success").equals("true")) {
//            ArrayList<LinkedTreeMap> listaPuntos = (ArrayList<LinkedTreeMap>)mapPuntosService.get("list");
//            Map<String, Object> newResponse = new HashMap<String, Object>();

//            if (!listaReglas.equals(null) & listaReglas.size() > 0) {
//                newResponse.put("success", true);
//                newResponse.put("list", listaReglas);
//            } else {
//                newResponse.put("success", false);
//                newResponse.put("code", 202);
//                newResponse.put("message", "THERE ARE NO HISTORY ASSOCIATED TO THIS TRACKER");
//                System.out.println("INFO: THERE ARE NO HISTORY ASSOCIATED TO THIS TRACKER");
//            }
                return gson.toJson(mapPuntosService);
            } else {
                return gson.toJson(mapPuntosService);
            }
        } else {
            return gson.toJson(userInfo);
        }

    }

//=====================================================================================================
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("reglas/lista/{hash}")
    public String reglasLista(@PathParam("hash") String hash) {

        Gson gson = new Gson();

        Map<String, Object> userInfo = navixyGetUserInfo(hash);
        Object validateHashStr = userInfo.get("success");
        if ((validateHashStr != null) & (String.valueOf(validateHashStr).equals("true"))) {
            Map<String, Object> mapReglasService = navixyRuleList(hash);
            if (mapReglasService != null & ((Boolean) mapReglasService.get("success"))) {
                //ArrayList<LinkedTreeMap> listaReglas = (ArrayList<LinkedTreeMap>)mapReglasService.get("list");

//            if (!listaReglas.equals(null) & listaReglas.size() > 0) {
//                newResponse.put("success", true);
//                newResponse.put("list", listaReglas);
//            } else {
//                newResponse.put("success", false);
//                newResponse.put("code", 202);
//                newResponse.put("message", "THERE ARE NO HISTORY ASSOCIATED TO THIS TRACKER");
//                System.out.println("INFO: THERE ARE NO HISTORY ASSOCIATED TO THIS TRACKER");
//            }
                return gson.toJson(mapReglasService);
            } else {
                return gson.toJson(mapReglasService);
            }
        } else {
            return gson.toJson(userInfo);
        }

    }

//=====================================================================================================
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("reglas/zona/{hash}/{zonaid}")
    public String reglasZona(@PathParam("hash") String hash, @PathParam("zonaid") String zonaid) {

        Gson gson = new Gson();

        Map<String, Object> userInfo = navixyGetUserInfo(hash);
        Object validateHashStr = userInfo.get("success");
        if ((validateHashStr != null) & (String.valueOf(validateHashStr).equals("true"))) {
            Map<String, Object> mapReglasService = navixyRuleList(hash);
            if (mapReglasService != null & ((Boolean) mapReglasService.get("success"))) {
                ArrayList<LinkedTreeMap> listaReglas = (ArrayList<LinkedTreeMap>) mapReglasService.get("list");
                Map<String, Object> newResponse = new HashMap<String, Object>();
                for (int i = 0; i < listaReglas.size(); i++) {
                    if (utilDoubleIntStr(listaReglas.get(i).get("zone_id")).equals(zonaid)) {
                        newResponse.put("zone_id", zonaid);
                        newResponse.put("schedule", listaReglas.get(i).get("schedule"));
                        newResponse.put("success", true);
                        break;
                    }
                }
                if (newResponse.size() == 0) {
                    newResponse.put("success", false);
                    newResponse.put("message", "No hay zona con ese id");
                }
//            if (!listaReglas.equals(null) & listaReglas.size() > 0) {
//                newResponse.put("success", true);
//                newResponse.put("list", listaReglas);
//            } else {
//                newResponse.put("success", false);
//                newResponse.put("code", 202);
//                newResponse.put("message", "THERE ARE NO HISTORY ASSOCIATED TO THIS TRACKER");
//                System.out.println("INFO: THERE ARE NO HISTORY ASSOCIATED TO THIS TRACKER");
//            }

                return gson.toJson(newResponse);
            } else {
                return gson.toJson(mapReglasService);
            }
        } else {
            return gson.toJson(userInfo);
        }

    }

//=====================================================================================================
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("entradascamion/lista/{hash}/{trackerid}/{fechaini}/{fechafin}")
    public String entradasCamionLista(@PathParam("hash") String hash, @PathParam("trackerid") String trackerid, @PathParam("fechaini") String fechaini, @PathParam("fechafin") String fechafin) {

        Gson gson = new Gson();

        Map<String, Object> userInfo = navixyGetUserInfo(hash);
        Object validateHashStr = userInfo.get("success");
        if ((validateHashStr != null) & (String.valueOf(validateHashStr).equals("true"))) {
            Map<String, Object> mapHistoricoService = navixyHistoryTrackerList(hash, "[" + trackerid + "]", fechaini, fechafin, "[\"inzone\",\"outzone\"]");
            if (mapHistoricoService != null & mapHistoricoService.get("success").equals("true")) {
                ArrayList<LinkedTreeMap> historicoReglasTrackTreeMaps = (ArrayList<LinkedTreeMap>) mapHistoricoService.get("list");
                Map<String, Object> newResponse = new HashMap<String, Object>();

                if (!historicoReglasTrackTreeMaps.equals(null) & historicoReglasTrackTreeMaps.size() > 0) {
                    newResponse.put("success", true);
                    newResponse.put("list", historicoReglasTrackTreeMaps);
                } else {
                    newResponse.put("success", false);
                    newResponse.put("code", 202);
                    newResponse.put("message", "THERE ARE NO HISTORY ASSOCIATED TO THIS TRACKER");
                    System.out.println("INFO: THERE ARE NO HISTORY ASSOCIATED TO THIS TRACKER");
                }

                return gson.toJson(newResponse);
            } else {
                return gson.toJson(mapHistoricoService);
            }
        } else {
            return gson.toJson(userInfo);
        }

    }

//=====================================================================================================
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("camion/ver/{hash}/{tracker}")
    //public String getJson(@PathParam("hash") String hash, @PathParam("tracker") String tracker) throws IOException, NoSuchAlgorithmException {
    public String camionVer(@PathParam("hash") String hash, @PathParam("tracker") String tracker) throws IOException, NoSuchAlgorithmException {

//        LogApp obj = new LogApp();
//        obj.runMe("Log ");
        Gson gson = new Gson();

        Map<String, Object> userInfo = navixyGetUserInfo(hash);
        Object validateHashStr = null;
        validateHashStr = userInfo.get("success");
        if ((validateHashStr != null) & (String.valueOf(validateHashStr).equals("true"))) {

//TODO: Captura Error
            NavixyTracker navixyTracker = null;

//            Map<String, Object> trackerLocation = null;
            Map<String, Object> trackerState = null;

// TRACKERS #####################################
            Map<String, Object> navixyResponseMap = navixyGetTrackers(hash);                                                    // Recibo el Mapa de Respuesta del Servicio
            String apiJsonOutput = gson.toJson(navixyResponseMap);                                                              // Convierto a String JSON el Mapa
            NavixyJsonResponse jsonResponse = gson.fromJson(apiJsonOutput, new TypeToken<NavixyJsonResponse>() {
            }.getType());                                                                                                       // Convierto el JSON a Objeto Respuesta
            ArrayList<Object> tempObjectList = jsonResponse.getList();                                                          // Le saco la lista de respuesta al Objeto
            apiJsonOutput = gson.toJson(tempObjectList);                                                                        // Convierto a Lista a String JSON
            ArrayList<NavixyTracker> navixyTrackers = new Gson().fromJson(apiJsonOutput, new TypeToken<ArrayList<NavixyTracker>>() {
            }.getType());                                                                                                       // Convierto el JSON a Objeto Lista de Trackers

            if (!navixyTrackers.equals(null) & navixyTrackers.size() > 0) {

                for (int j = 0; j < navixyTrackers.size(); j++) {
                    navixyTracker = navixyTrackers.get(j);
                    String trackerID = navixyTracker.getId();
                    if (trackerID.equals(tracker)) {     // EL TRACKER COINCIDE CON EL SUMINISTRADO POR PARAMETRO  

//                        trackerLocation = navixyTrackerGetLocation(hash, trackerID);    // DEBE DAR LA LECTURA GPS/GSM DEL TRACKER
                        trackerState = navixyTrackerGetState(hash, trackerID);          // DEBE DAR LAS LECTURAS CUSTOMIZADAS DEL OBD

                        //$$$ TODO: Hacerlo eficiente
                        //$$$ break;
                    }
                }
            }

            if (trackerState == null) {
                System.out.println("INFO: THERE ARE NO TRACKERS ASSOCIATED TO THIS ACCOUNT");
                String responseJson = "{\"success\":false,"
                        + "\"status\":{\"code\":5,\"description\":\"No Tracker Data\" }"
                        + "}";
                trackerState = gson.fromJson(responseJson, new TypeToken<Map<String, Object>>() {
                }.getType());    // Convierto el JSON a Objeto Respuesta

                return gson.toJson(trackerState);
            } else {
                Map<String, Object> response = new HashMap<>();
                //response.put("success",true);
                response.put("label", navixyTracker.getLabel());

                //response.put("group", "GROUP:" + navixyTracker.getGroup_id().toString());
                trackerState.putAll(response);

                return gson.toJson(trackerState);
            }
        } else {
            return gson.toJson(userInfo);
        }
    }

//=====================================================================================================
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("zonas/camiones/{hash}")
    public String zonasCamiones(@PathParam("hash") String hash) throws IOException, NoSuchAlgorithmException {

        Gson gson = new Gson();

// Validacion de Hash         
        Map<String, Object> userInfo = navixyGetUserInfo(hash);
        Object validateHashStr = null;
        validateHashStr = userInfo.get("success");
        if ((validateHashStr != null) & (String.valueOf(validateHashStr).equals("true"))) {

// Proceso la Lista de Grupos como LinkedTreeMap
            //ArrayList<LinkedTreeMap> treeMapGrupos = (ArrayList<LinkedTreeMap>) navixyTrackerGetGroupList(hash).get("list");
// Proceso la Lista de Zonas como LinkedTreeMap            
            ArrayList<LinkedTreeMap> listTreeMapZonas = (ArrayList<LinkedTreeMap>) navixyGetZoneList(hash).get("list");
// Lista de los Grupos de Camiones
//            ArrayList<LinkedTreeMap> treeMapCamiones = (ArrayList<LinkedTreeMap>) navixyGetTrackers(hash).get("list");
//Lista de Reglas de Alerta de Trackers        
            ArrayList<LinkedTreeMap> alertsNavixyTreeMap = (ArrayList<LinkedTreeMap>) navixyRuleList(hash).get("list");
// Colector de Grupos / Camiones
            Map<String, Set<String>> gruposTrackersMap = new HashMap<>();
// Antiguo colector 
            Map<String, Object> newResponse = new HashMap<String, Object>();

            alertsNavixyTreeMap.forEach((alertas)
                    -> {
                Set<String> trackers;
                String zonaId = utilDoubleIntStr(alertas.get("zone_id")); // se pide la geocerca de la zona. Si es Cero, no tiene y no nos interesa

                // si la zona es 0 la alerta no usa geocerca
                if (!zonaId.equals("0")) {

                    // Busco el nombre de la zona
                    String nombreZona = "";
                    for (int i = 0; i < listTreeMapZonas.size(); i++) {
                        LinkedTreeMap zona = listTreeMapZonas.get(i);
                        if (utilDoubleIntStr(zona.get("id")).equals(zonaId)) {
                            nombreZona = (String) zona.get("label");
                            break;
                        }
                    }

                    // Si en el colector ya fue incluida la zona la tomamos sino la creamos
                    if (gruposTrackersMap.containsKey(nombreZona)) {
                        trackers = gruposTrackersMap.get(nombreZona);
                    } else {
                        trackers = new HashSet<>();
                    }

                    // En el SET trackers voy insertando Sin Repeticion los ID de trackers
                    ArrayList<Double> ff = (ArrayList<Double>) alertas.get("trackers");
                    ff.forEach((trackerId) -> {
                        //String fff= (String) trackerId;
                        trackers.add(utilDoubleIntStr(trackerId));
                    });

                    gruposTrackersMap.put(nombreZona, trackers);
                }
            });

//            if (!treeMapGrupos.equals(null) & treeMapGrupos.size() > 0) {
//                treeMapGrupos.forEach((n) -> {
//                    ArrayList camiones = new ArrayList();
//                    for (int i = 0; i < treeMapCamiones.size(); i++) {
//                        if (utilDoubleIntStr(n.get("id")).equals(utilDoubleIntStr(treeMapCamiones.get(i).get("group_id")))) {
//                            camiones.add(treeMapCamiones.get(i));
//                        }
//                    }
//                    n.put("trackers", camiones);
//                });
//
//            } else {
//                System.out.println("INFO: THERE ARE NO GROUPS ASSOCIATED TO THIS ACCOUNT");
//            }
            newResponse.put("success", true);
            newResponse.put("list", gruposTrackersMap);
            return gson.toJson(newResponse);
        } else {
            return gson.toJson(userInfo);
        }

    }

//=====================================================================================================
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("camiones/zona/{hash}/{zonaid}")
    public String camionesPorZona(@PathParam("hash") String hash, @PathParam("zonaid") String zonaid) throws IOException, NoSuchAlgorithmException {

        Gson gson = new Gson();

// Validacion de Hash         
        Map<String, Object> userInfo = navixyGetUserInfo(hash);
        Object validateHashStr = null;
        validateHashStr = userInfo.get("success");
        if ((validateHashStr != null) & (String.valueOf(validateHashStr).equals("true"))) {

// Proceso la Lista de Grupos como LinkedTreeMap
            //ArrayList<LinkedTreeMap> treeMapGrupos = (ArrayList<LinkedTreeMap>) navixyTrackerGetGroupList(hash).get("list");
// Proceso la Lista de Zonas como LinkedTreeMap            
            ArrayList<LinkedTreeMap> listTreeMapZonas = (ArrayList<LinkedTreeMap>) navixyGetZoneList(hash).get("list");
// Lista de los Grupos de Camiones
//            ArrayList<LinkedTreeMap> treeMapCamiones = (ArrayList<LinkedTreeMap>) navixyGetTrackers(hash).get("list");
//Lista de Reglas de Alerta de Trackers        
            ArrayList<LinkedTreeMap> alertsNavixyTreeMap = (ArrayList<LinkedTreeMap>) navixyRuleList(hash).get("list");
            alertsNavixyTreeMap.removeIf(n -> (!utilDoubleIntStr(n.get("zone_id")).equals(zonaid)));
            //x.removeIf(n -> (!n.get("type").equals("polygon")));

// Colector de Grupos / Camiones
            Map<String, Set<String>> gruposTrackersMap = new HashMap<>();
// Antiguo colector 
            Map<String, Object> newResponse = new HashMap<String, Object>();

            alertsNavixyTreeMap.forEach((alertas)
                    -> {
                Set<String> trackers;
                String zonaId = utilDoubleIntStr(alertas.get("zone_id")); // se pide la geocerca de la zona. Si es Cero, no tiene y no nos interesa

                // si la zona es 0 la alerta no usa geocerca
                if (!zonaId.equals("0")) {

                    // Busco el nombre de la zona
                    String nombreZona = "";
                    for (int i = 0; i < listTreeMapZonas.size(); i++) {
                        LinkedTreeMap zona = listTreeMapZonas.get(i);
                        if (utilDoubleIntStr(zona.get("id")).equals(zonaId)) {
                            nombreZona = (String) zona.get("label");
                            break;
                        }
                    }

                    // Si en el colector ya fue incluida la zona la tomamos sino la creamos
                    if (gruposTrackersMap.containsKey(nombreZona)) {
                        trackers = gruposTrackersMap.get(nombreZona);
                    } else {
                        trackers = new HashSet<>();
                    }

                    // En el SET trackers voy insertando Sin Repeticion los ID de trackers
                    ArrayList<Double> ff = (ArrayList<Double>) alertas.get("trackers");
                    ff.forEach((trackerId) -> {
                        //String fff= (String) trackerId;
                        trackers.add(utilDoubleIntStr(trackerId));
                    });

                    //gruposTrackersMap.put(nombreZona, trackers);
                    gruposTrackersMap.put("list", trackers);
                }
            });

//            if (!treeMapGrupos.equals(null) & treeMapGrupos.size() > 0) {
//                treeMapGrupos.forEach((n) -> {
//                    ArrayList camiones = new ArrayList();
//                    for (int i = 0; i < treeMapCamiones.size(); i++) {
//                        if (utilDoubleIntStr(n.get("id")).equals(utilDoubleIntStr(treeMapCamiones.get(i).get("group_id")))) {
//                            camiones.add(treeMapCamiones.get(i));
//                        }
//                    }
//                    n.put("trackers", camiones);
//                });
//
//            } else {
//                System.out.println("INFO: THERE ARE NO GROUPS ASSOCIATED TO THIS ACCOUNT");
//            }
            newResponse.put("success", true);
            newResponse.put("list", gruposTrackersMap.get("list"));
            return gson.toJson(newResponse);
            //return gson.toJson(newResponse);
        } else {
            return gson.toJson(userInfo);
        }

    }

//=====================================================================================================
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("camion/lista/{hash}")
    public String camionLista(@PathParam("hash") String hash) throws IOException, NoSuchAlgorithmException {

        Gson gson = new Gson();

        Map<String, Object> userInfo = navixyGetUserInfo(hash);
        Object validateHashStr = null;
        validateHashStr = userInfo.get("success");
        if ((validateHashStr != null) & (String.valueOf(validateHashStr).equals("true"))) {
// TRACKERS #####################################
            Map<String, Object> navixyResponseMap = navixyGetTrackers(hash);                                                    // Recibo el Mapa de Respuesta del Servicio
            String apiJsonOutput = gson.toJson(navixyResponseMap);                                                              // Convierto a String JSON el Mapa
            NavixyJsonResponse jsonResponse = gson.fromJson(apiJsonOutput, new TypeToken<NavixyJsonResponse>() {
            }.getType());    // Convierto el JSON a Objeto Respuesta
            ArrayList<Object> tempObjectList = jsonResponse.getList();                                                          // Le saco la lista de respuesta al Objeto
            apiJsonOutput = gson.toJson(tempObjectList);                                                                        // Convierto a Lista a String JSON
            ArrayList<NavixyTracker> navixyTrackers = new Gson().fromJson(apiJsonOutput, new TypeToken<ArrayList<NavixyTracker>>() {
            }.getType());                                                                                                       // Convierto el JSON a Objeto Lista de Trackers

            Map<String, Object> newTrackerResponse = new HashMap<String, Object>();

            if (!navixyTrackers.equals(null) & navixyTrackers.size() > 0) {

                newTrackerResponse.put("success", true);
                ArrayList trackerList = new ArrayList<Map<String, String>>();

                for (int j = 0; j < navixyTrackers.size(); j++) {
                    Map<String, String> newTracker = new HashMap<String, String>();
                    newTracker.put("label", navixyTrackers.get(j).getLabel());
                    newTracker.put("id", navixyTrackers.get(j).getId());
                    trackerList.add(newTracker);
                }

                newTrackerResponse.put("list", trackerList);

                return gson.toJson(newTrackerResponse);

            } else {

                System.out.println("INFO: THERE ARE NO TRACKERS ASSOCIATED TO THIS ACCOUNT");

                newTrackerResponse.put("success", false);
                newTrackerResponse.put("message", "No hay camiones");

                return gson.toJson(newTrackerResponse);

            }

        } else {
            return gson.toJson(userInfo);
        }

    }

//=====================================================================================================
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("zonas/lista/{hash}")
    public String zonasLista(@PathParam("hash") String hash) throws IOException, NoSuchAlgorithmException {

        Gson gson = new Gson();

        Map<String, Object> userInfo = navixyGetUserInfo(hash);
        Object validateHashStr = null;
        validateHashStr = userInfo.get("success");
        if ((validateHashStr != null) & (String.valueOf(validateHashStr).equals("true"))) {

            Map<String, Object> navixyResponseGeoCercasMap = navixyGetZoneList(hash);   // Lista las GeoCercas        

            ArrayList<LinkedTreeMap> x = (ArrayList<LinkedTreeMap>) navixyResponseGeoCercasMap.get("list");

            Map<String, Object> newResponse = new HashMap<String, Object>();

            if (!navixyResponseGeoCercasMap.equals(null) & navixyResponseGeoCercasMap.size() > 0) {

                //x = (ArrayList<LinkedTreeMap>) navixyResponseGeoCercasMap.get("list");
                // Removemos del arreglo los que no son poligono
                x.removeIf(n -> (!n.get("type").equals("polygon")));
                x.removeIf(n -> (!((String) n.get("label")).contains("RUTA ")));

                x.forEach((n) -> {
                    n.put("coords", navixyGetZonePointsList(hash, "" + ((Double) n.get("id")).intValue()).get("list"));
                });

                newResponse.put("success", true);
                newResponse.put("list", x);
            } else {
                newResponse.put("success", false);
                newResponse.put("code", 202);
                newResponse.put("message", "THERE ARE NO TRACKERS ASSOCIATED TO THIS ACCOUNT");
                System.out.println("INFO: THERE ARE NO TRACKERS ASSOCIATED TO THIS ACCOUNT");
            }

            return gson.toJson(newResponse);
        } else {
            return gson.toJson(userInfo);
        }

    }

//=====================================================================================================
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("sectores/poligonos/{hash}/{sectorid}")
    public String sectoresPoligonos(@PathParam("hash") String hash, @PathParam("sectorid") String sectorid) throws IOException, NoSuchAlgorithmException {

        Gson gson = new Gson();

        Map<String, Object> userInfo = navixyGetUserInfo(hash);
        Object validateHashStr = null;
        validateHashStr = userInfo.get("success");
        if ((validateHashStr != null) & (String.valueOf(validateHashStr).equals("true"))) {

            Map<String, Object> navixyResponseGeoCercasMap = navixyGetZoneList(hash);   // Lista las GeoCercas        
            Map<String, Object> newResponse = new HashMap<String, Object>();
            if ((navixyResponseGeoCercasMap != null) & (navixyResponseGeoCercasMap.size() > 0) & ((Boolean) navixyResponseGeoCercasMap.get("success"))) {

//CONSTRUYO EL MAPA DE RELACION RUTA NOMBRE -> ID DE RUTA  +  SELECCION DE SECTOR 
                ArrayList<LinkedTreeMap> listGeocercasTreeMap = (ArrayList<LinkedTreeMap>) navixyResponseGeoCercasMap.get("list");
                Map<String, String> rutasMap = new HashMap<String, String>();
                LinkedTreeMap sector = null;
                for (int i = 0; i < listGeocercasTreeMap.size(); i++) {
                    LinkedTreeMap geoCerca = listGeocercasTreeMap.get(i);
                    if (((String) geoCerca.get("label")).contains("RUTA ")) {
                        String key = (String) geoCerca.get("label");
                        Double val = (Double) geoCerca.get("id");
                        rutasMap.put(key, utilDoubleIntStr(val));
                    } else if (utilDoubleIntStr((Double) geoCerca.get("id")).equals(sectorid)) {
                        sector = geoCerca;
                    }
                }

// Si no hay sector, retornamos rerror 
                if (sector == null) {
                    newResponse.put("success", false);
                    newResponse.put("code", 203);
                    newResponse.put("message", "THERE ARE NO SUCH SECTOR ID ASSOCIATED TO THIS ACCOUNT");
                    return gson.toJson(newResponse);
                }

//CONSTRUYO EL MAPA DE RELACION TAG -> NOMBRE GEOCERCA (RUTA / SECTOR / OTRO)    
                Map<String, Object> tagsResponseMap = NavixyAPICalls.navixyTagList(hash);
                ArrayList<LinkedTreeMap> tagsList = (ArrayList<LinkedTreeMap>) tagsResponseMap.get("list");
                Map<String, String> tagsMap = new HashMap<String, String>();
                for (int i = 0; i < tagsList.size(); i++) {
                    LinkedTreeMap etiqueta = tagsList.get(i);
                    Double key = (Double) etiqueta.get("id");
                    String val = (String) etiqueta.get("name");
                    tagsMap.put(utilDoubleIntStr(key), val);
                }

//SI EL SECTOR TIENE SU TAG CONSRUYO LA SALIDA            
                if (sector.containsKey("tags")) {
                    ArrayList tagsSector = (ArrayList) sector.get("tags");
                    Double primerTagSector = (Double) tagsSector.get(0);
                    String strPrimerTag = utilDoubleIntStr(primerTagSector);
                    String strNombreRuta = tagsMap.get(strPrimerTag);
                    String strIdRuta = rutasMap.get(strNombreRuta);

                    newResponse.put("success", true);
                    newResponse.put("sectorcoords", navixyGetZonePointsList(hash, sectorid).get("list"));
                    newResponse.put("rutacoords", navixyGetZonePointsList(hash, strIdRuta).get("list"));
                    return gson.toJson(newResponse);
                } else {
                    newResponse.put("success", false);
                    newResponse.put("code", 205);
                    newResponse.put("message", "THERE ARE NO TAG ASSOCIATED TO THIS SECTOR");
                    return gson.toJson(newResponse);
                }

            } else {
                newResponse.put("success", false);
                newResponse.put("code", 202);
                newResponse.put("message", "THERE ARE NO TRACKERS ASSOCIATED TO THIS ACCOUNT");
                System.out.println("INFO: THERE ARE NO TRACKERS ASSOCIATED TO THIS ACCOUNT");
            }

            return gson.toJson(newResponse);
        } else {
            return gson.toJson(userInfo);
        }

    }

//=====================================================================================================
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("sectores/lista/{hash}")
    public String sectoresLista(@PathParam("hash") String hash) throws IOException, NoSuchAlgorithmException {

        Gson gson = new Gson();

        Map<String, Object> userInfo = navixyGetUserInfo(hash);
        Object validateHashStr = null;
        validateHashStr = userInfo.get("success");
        if ((validateHashStr != null) & (String.valueOf(validateHashStr).equals("true"))) {

            Map<String, Object> navixyResponseGeoCercasMap = navixyGetZoneList(hash);   // Lista las GeoCercas        

            ArrayList<LinkedTreeMap> x = (ArrayList<LinkedTreeMap>) navixyResponseGeoCercasMap.get("list");

            Map<String, String> rutasMap = new HashMap<String, String>();

//            for(int i=0;i<x.size();i++){
//                 
//                 LinkedTreeMap ruta = x.get(i);//.get("tags")).get(0);
//                 if(((String)ruta.get("label")).contains("RUTA ")) {
//                     String key = (String) ruta.get("label");
//                     Double val = (Double) ruta.get("id");
//                     rutasMap.put(key,utilDoubleIntStr(val));
//                 }
//                 
//                // zonasMap.put(((LinkedHashMap)zonas.get(i).get("tags")).get("id"), hash)
//            }
            Map<String, Object> newResponse = new HashMap<String, Object>();

            if (!navixyResponseGeoCercasMap.equals(null) & navixyResponseGeoCercasMap.size() > 0) {

                //x = (ArrayList<LinkedTreeMap>) navixyResponseGeoCercasMap.get("list");
                // Removemos del arreglo los que no son poligono
                x.removeIf(n -> (!((String) n.get("label")).contains("SECTOR ")));

                x.forEach((n) -> {
                    n.put("coords", navixyGetZonePointsList(hash, "" + ((Double) n.get("id")).intValue()).get("list"));
                });

                newResponse.put("success", true);
                newResponse.put("list", x);
            } else {
                newResponse.put("success", false);
                newResponse.put("code", 202);
                newResponse.put("message", "THERE ARE NO TRACKERS ASSOCIATED TO THIS ACCOUNT");
                System.out.println("INFO: THERE ARE NO TRACKERS ASSOCIATED TO THIS ACCOUNT");
            }

            return gson.toJson(newResponse);
        } else {
            return gson.toJson(userInfo);
        }

    }

//=====================================================================================================
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("sectores/ruta/{hash}")
    public String sectoresRuta(@PathParam("hash") String hash) throws IOException, NoSuchAlgorithmException {

        Gson gson = new Gson();

        Map<String, Object> userInfo = navixyGetUserInfo(hash);
        Object validateHashStr = null;
        validateHashStr = userInfo.get("success");
        if ((validateHashStr != null) & (String.valueOf(validateHashStr).equals("true"))) {

            Map<String, Object> navixyResponseGeoCercasMap = navixyGetZoneList(hash);   // Lista las GeoCercas   

            Map<String, Object> rutasServiceMap = new HashMap<String, Object>();
            rutasServiceMap.putAll(navixyResponseGeoCercasMap);

            ArrayList<LinkedTreeMap> geoCercasList = (ArrayList<LinkedTreeMap>) navixyResponseGeoCercasMap.get("list");
            ArrayList<LinkedTreeMap> rutasList = new ArrayList<>();

            rutasList = (ArrayList<LinkedTreeMap>) rutasServiceMap.get("list");
            //rutasList.removeIf(n -> (!((String)n.get("label")).contains("RUTA ")));           
            Map<String, String> rutasMap = new HashMap<String, String>();

            for (int i = 0; i < rutasList.size(); i++) {

                LinkedTreeMap ruta = rutasList.get(i);//.get("tags")).get(0);
                if (((String) ruta.get("label")).contains("RUTA ")) {
                    String key = (String) ruta.get("label");
                    Double val = (Double) ruta.get("id");
                    rutasMap.put(key, utilDoubleIntStr(val));
                }

                // zonasMap.put(((LinkedHashMap)zonas.get(i).get("tags")).get("id"), hash)
            }

            Map<String, Object> newResponse = new HashMap<String, Object>();

            Map<String, Object> tagsResponseMap = NavixyAPICalls.navixyTagList(hash);

            ArrayList<LinkedTreeMap> tagsList = (ArrayList<LinkedTreeMap>) tagsResponseMap.get("list");

            Map<String, String> tagsMap = new HashMap<String, String>();

            for (int i = 0; i < tagsList.size(); i++) {

                LinkedTreeMap etiqueta = tagsList.get(i);//.get("tags")).get(0);
                //if(etiqueta.containsKey("tags")) {
                Double key = (Double) etiqueta.get("id");
                String val = (String) etiqueta.get("name");
                tagsMap.put(utilDoubleIntStr(key), val);
                //}

                // zonasMap.put(((LinkedHashMap)zonas.get(i).get("tags")).get("id"), hash)
            }

            if (!navixyResponseGeoCercasMap.equals(null) & navixyResponseGeoCercasMap.size() > 0) {

                //x = (ArrayList<LinkedTreeMap>) navixyResponseGeoCercasMap.get("list");
                // Removemos del arreglo los que no son poligono
                geoCercasList.removeIf(n -> (!((String) n.get("label")).contains("SECTOR ")));

                // Le añado el nombre de la ruta a cada sector
                geoCercasList.forEach((n) -> {
                    if (n.containsKey("tags")) {
                        ArrayList ee = (ArrayList) n.get("tags");
                        Double rrr = (Double) ee.get(0);
                        String sdsf = utilDoubleIntStr(rrr);
                        String ree = tagsMap.get(sdsf);
                        n.put("ruta", ree);
                        n.put("idruta", rutasMap.get(ree));
                    }
                    n.remove("address");
                    n.remove("color");
                    n.remove("type");
                    n.remove("tags");
                });

                newResponse.put("success", true);
                newResponse.put("list", geoCercasList);
            } else {
                newResponse.put("success", false);
                newResponse.put("code", 202);
                newResponse.put("message", "THERE ARE NO TRACKERS ASSOCIATED TO THIS ACCOUNT");
                System.out.println("INFO: THERE ARE NO TRACKERS ASSOCIATED TO THIS ACCOUNT");
            }

            return gson.toJson(newResponse);
        } else {
            return gson.toJson(userInfo);
        }

    }

//=====================================================================================================
//    
//
//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//@Path("ignore{hash}/{tracker}")
//    public String getJson(@PathParam("hash") String hash,@PathParam("tracker")  String tracker) throws IOException, NoSuchAlgorithmException {
//        //TODO return proper representation object
//        
//        Gson gson = new Gson();
//        Map<String, Object> userInfo = navixyGetUserInfo(hash);
//        Object validateHashStr = null;
//        validateHashStr = userInfo.get("success");
//        if ((validateHashStr != null) & (String.valueOf(validateHashStr).equals("true"))) {
//        Map<String, Object> trackerLocation = null;
//        Map<String, Object> trackerState = null;
//
//// TRACKERS #####################################
//        Map<String, Object> navixyResponseMap = navixyGetTrackers(hash);                                // Recibo el Mapa de Respuesta del Servicio
//        ArrayList<NavixyTracker> navixyTrackers = listNavixyTrackers(navixyResponseMap);
//
//        if ((navixyTrackers != null) & navixyTrackers.size() > 0) {
//
//            for (int j = 0; j < navixyTrackers.size(); j++) {
//                NavixyTracker navixyTracker = (NavixyTracker) navixyTrackers.get(j);
//                String trackerID = navixyTracker.getId();
//                String comp = tracker;
//                if (trackerID.equals(comp)) {
//                    trackerLocation = navixyTrackerGetLocation(hash,trackerID);
//                    trackerState = navixyTrackerGetDiagnostics(hash,trackerID);
//                    System.out.println("############  OBD TRACKER ");
//                }
//            }
//        } else {
//            System.out.println("INFO: THERE ARE NO TRACKERS ASSOCIATED TO THIS ACCOUNT");
//        }
//
//        if (trackerState == null) {
//                System.out.println("INFO: THERE ARE NO TRACKERS ASSOCIATED TO THIS ACCOUNT");
//                String responseJson = "{\"success\":false,"
//                        + "\"status\":{\"code\":5,\"description\":\"No Tracker Data\" }"
//                        + "}";
//                trackerState = gson.fromJson(responseJson, new TypeToken<Map<String, Object>>() {
//                }.getType());    // Convierto el JSON a Objeto Respuesta
//
//            }
//
//        gson = new Gson();
//
//        return gson.toJson(trackerState);
//        
//        } else {
//                return gson.toJson(userInfo);
//
//        }
//    }
    /**
     * PUT method for updating or creating an instance of GenericResource
     *
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public String putJson(String content) {
        return "GETTER";

    }
}
